import "./App.css";
import firebase from "./firebaseInit";
import React, { useState, useEffect } from "react";

const db = firebase.database();
const usersRef = db.ref("/users");
usersRef.on("value", (snapshot) => {
  console.log(snapshot.val());
});

function App() {
  const [name, setName] = useState("");
  const [msg, setMsg] = useState("");
  const [users, setUsers] = useState([]);

  useEffect(() => {
    usersRef.on("value", (snapshot) => {
      const data = [];

      snapshot.forEach((child) => {
        data.push({
          id: child.key,
          name: child.val().name,
        });
      });
      setUsers(data);
    });
    return () => usersRef.off();
  }, []);
  const submitHandler = () => {
    const usersData = usersRef.push();
    const addData = usersData.set({
      name,
      age: 20,
    });

    if (addData) {
      setMsg("berhasil input data");
    }
  };

  const deleteHandler = (key) => {
    usersRef.child(key).remove();
  };

  const Table = () => {
    return (
      <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>action</th>
          </tr>
        </thead>
        <tbody>
          {users &&
            users.map((user) => {
              return (
                <tr key={user.id}>
                  <td>{user.id}</td>
                  <td>{user.name}</td>
                  <td>
                    <button onClick={() => deleteHandler(user.id)}>
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    );
  };
  return (
    <div className="App">
      <Table />
      <br />
      {msg}
      <br />
      <input
        type="text"
        name="name"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <br />
      <button type="submit" onClick={submitHandler}>
        Add
      </button>
    </div>
  );
}

export default App;
