import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCPZEu0e9sywVT6QQLW_L27HL77VD1mkZ8",
  authDomain: "my-app-9c341.firebaseapp.com",
  projectId: "my-app-9c341",
  storageBucket: "my-app-9c341.appspot.com",
  messagingSenderId: "416842072239",
  appId: "1:416842072239:web:ddfefd738e71807fc3545a",
  measurementId: "G-Q5RS2CHK9H",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase;
